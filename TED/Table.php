<?php

namespace TED;

/**
 * TooEasyDatabase: Table Abstraction Layer
 * 
 * @copyright (c) 2015, Niko Hujanen
 * @version 1.00
 * @package TED
 */
class Table implements \ArrayAccess
{
    protected   $_cache     = null;
    protected   $_ted       = null;
    protected   $_table     = null;
    
    /**
     * Contruct \TED\Table
     * 
     * @param <\TED> $ted
     * @param <string> $tableName
     * @return <\TED\Table>
     */
    public function __construct(\TED &$ted, $tableName) 
    {
        $this->_ted     = $ted;
        $this->_table   = $tableName;
        
        return $this;
    }
    
    /**
     * Get table name
     * 
     * @return <string>
     */
    public function getName()
    {
        return $this->_table;
    }
    
    /**
     * Clean up array for saving
     * 
     * @param <array> $data
     * @return <array>
     */
    public function cleanupArray(array $data = array())
    {
        $this->columnExists('id');
        
        return array_intersect_key($data, $this->_cache);
    }
    
    /**
     * See that column exists in table
     * 
     * @param <string> $columnName
     * @return <bool>
     */
    public function columnExists($columnName)
    {
        if (is_null($this->_cache)) {
            foreach ($this->_ted->getAll(sprintf('DESCRIBE %s', $this->_ted->quoteIdentifier($this->_table))) as $column) {
                $this->_cache[$column['Field']] = $column['Field'];
            }
        }
        
        return array_key_exists($columnName, $this->_cache);
    }
    
    /**
     * Extended quoteInto with automatic table placeholder
     * 
     * @param <string> $sqlQuery    SQL query with placeholders (? -> value, @ -> table)
     * @param <string> $value       This value will be quoted before replace
     * @return <string>
     */
    public function quoteInto($sqlQuery, $value)
    {
        return $this->_ted->quoteInto(str_replace('@', $this->_ted->quoteIdentifier($this->_table), $sqlQuery), $value);
    }
    
    /**
     * Not allowed.
     * 
     * @throws <\TED\Exception>
     */
    private function __clone() 
    {
        throw new \TED\Exception('No cloning.');
    }
    
    /**
     * See if record with primary key exists.
     * 
     * @param <mixed> $primaryKey
     * @return <bool>
     */
    public function offsetExists($primaryKey) 
    {
        return is_array($this->_ted->getRow($this->quoteInto('SELECT 1 FROM @ WHERE @.id = ? LIMIT 1', (string) $primaryKey)));        
    }
    
    /**
     * Get record with primary key
     * 
     * @param <string> $primaryKey
     * @throws <\TED\Exception>
     * @return <\TED\Entry>
     */
    public function offsetGet($primaryKey) 
    {
        $data = $this->getRow($primaryKey);
        if (is_array($data)) {
            return new \TED\Entry($this, $primaryKey, $data);
        } else {
            throw new \TED\Exception("No entry for {PK='{$primaryKey}'} in table '{$this->_table}'");
        }
    }
    
    /**
     * Set record with primary key
     * 
     * @param <string|null> $primaryKey     <null> -> insert, <string> -> update
     * @param <\TED\Entry|array> $data
     * @return <bool>
     */
    public function offsetSet($primaryKey, $data) 
    {
        $data = ($data instanceof \TED\Entry) ? $data->toArray() : $data;
        if (is_null($primaryKey)) {
            $this->_ted->insert($this->_table, $data);
            return $this->_ted->lastID();
        } else {
            $this->_ted->update($this->_table, $data, $this->quoteInto('@.id = ?', $primaryKey));
            return $primaryKey;
        }
    }
    
    /**
     * Delete record with primary key
     * 
     * @param <string> $primaryKey
     * @return <bool>
     */
    public function offsetUnset($primaryKey) 
    {
        return $this->_ted->delete($this->_table, $this->quoteInto('@.id = ?', $primaryKey));
    }
    
    /**
     * Factorize new entry for this table
     * 
     * @param <array> $data
     * @return <\TED\Entry>
     */
    public function entry(array $data = array())
    {
        return new \TED\Entry($this, null, $data);
    }
    
    /**
     * Get row from current table with given primary key
     * 
     * @param <string> $primaryKey
     * @return <array>
     */
    public function getRow($primaryKey)
    {
        return $this->_ted->getRow($this->quoteInto('SELECT * FROM @ WHERE @.id = ? LIMIT 1', (string) $primaryKey));
    }
    
}