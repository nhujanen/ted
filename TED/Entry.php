<?php

namespace TED;

/**
 * TooEasyDatabase: Row Abstraction Layer
 * 
 * @copyright (c) 2015, Niko Hujanen
 * @version 1.00
 * @package TED
 */
class Entry {
    protected   $_table = null;
    protected   $_pk    = null;
    protected   $_data  = array();
    
    /**
     * Contructor
     * 
     * @param <\TED> $ted
     * @param <string> $tableName
     * @param <string> $primaryKey
     * @param <array> $data
     * @return <\TED\Entry>
     */
    public function __construct(\TED\Table &$tedTable, $primaryKey = null, array $data = array())
    {
        $this->_table   = $tedTable;
        $this->_pk      = $primaryKey;
        $this->_data    = array_diff_key($data, array('id' => null));
        
        return $this;
    }
    
    /**
     * Not allowed.
     * 
     * @throws <\TED\Exception>
     */
    private function __clone() {
        throw new \TED\Exception('No cloning.');
    }
    
    /**
     * Save current entry
     * 
     * @return <string> Primary key
     */
    public function save()
    {
        $data = $this->_table->cleanupArray($this->_data);
        $this->_pk = $this->_table->offsetSet($this->_pk, $data);
        $this->reload();                
        
        return $this->_pk;
    }
    
    /**
     * Reload current entry
     * 
     * @return <\TED\Entry> Current entry
     */
    public function reload()
    {
        $this->_data = array_diff_key($this->_table->getRow($this->_pk), array('id' => null));
        
        return $this;
    }
    
    /**
     * Delete current entry
     * 
     * @throws <\TED\Exception>
     * @return <bool>
     */
    public function delete()
    {
        if (is_null($this->_pk)) {
            throw new \TED\Exception("Deleting without primary key is not possible.");
        } else {
            return $this->_table->offsetUnset($this->_pk);
        }
    }
    
    /**
     * Get entry's content as array
     * 
     * @return <array>
     */
    public function toArray()
    {
        return $this->_data;
    }
    
    /**
     * Set value to entry
     * 
     * @param <string> $name
     * @param <mixed> $value
     * @return <bool>
     */
    public function __set($name, $value) 
    {
        return ($this->_data[$name] = $value);
    }
    
    /**
     * Get value from entry
     * 
     * @param <string> $name
     * @return <mixed>
     */
    public function __get($name) 
    {
        if ($name === 'id') return $this->_pk;
        
        return array_key_exists($name, $this->_data) ? $this->_data[$key] : null;
    }
    
}