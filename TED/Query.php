<?php

namespace TED;

/**
 * TooEasyDatabase: Query builder
 * 
 * @copyright (c) 2015, Niko Hujanen
 * @version 1.00
 * @package TED
 */
class Query implements \Iterator {
    protected   $_ted       = null;
    protected   $_table     = null;
    protected   $_smt       = null;
    protected   $_row       = null;
    protected   $_iterator  = null;
    protected   $_parts     = array(
        'columns'   => array(),
        'where'     => array(),
        'group'     => array(),
        'order'     => array(),
        'join'      => array(),
    );
    
    /**
     * Constructor
     * 
     * @param <\TED> $ted
     * @param <\TED\Table> $tableName
     * @return <\TED\Query>
     */
    public function __construct(\TED &$ted, \TED\Table &$table)
    {
        $this->_ted     = $ted;
        $this->_table   = $table;
        $this->_parts['columns'][] = $table->getName() . ".*";
        
        return $this;
    }
    
    /**
     * Reset part of query
     * 
     * @param <string> $part
     * @return <\TED\Query>
     */
    public function reset($part)
    {
        $this->_parts[$part] = array();
        
        return $this;
    }
    
    /**
     * Add columns to query
     * 
     * @param <array|string> $columns
     * @return <\TED\Query>
     */
    public function columns($columns)
    {
        $columns = is_array($columns) ? $columns : array($columns);
        foreach ($columns as $column) {
            $this->_parts['columns'][] = $column;
        }
        
        return $this;
    }
    
    /**
     * Add where rules to query
     * 
     * @param <string> $sqlCondition
     * @param <string|null> $value
     * @return <\TED\Query>
     */
    public function where($sqlCondition, $value = null)
    {
        $this->_parts['where'][] = is_null($value) ? $sqlCondition : str_replace('?', $this->_ted->quote($value), $sqlCondition);
        
        return $this;
    }
    
    /**
     * Add group bys to query
     * 
     * @param <string> $columnName
     * @return <\TED\Query>
     */
    public function group($columnName)
    {
        $this->_parts['group'][] = $columnName;
        
        return $this;
    }
    
    /**
     * Add order bys to query
     * 
     * @param <string> $sqlOrder
     * @return <\TED\Query>
     */
    public function order($sqlOrder)
    {
        $this->_parts['order'][] = $sqlOrder;
        
        return $this;
    }
    
    /**
     * Add left join to query
     * 
     * @param <string> $tableName
     * @param <string|array> $joinRules
     * @param <array> $columns
     * @return <\TED\Query>
     */
    public function joinLeft($tableName, $joinRules, array $columns = array())
    {
        $this->_parts['join'][] = array(
            'type'      => 'LEFT JOIN',
            'table'     => $tableName,
            'on'        => is_array($joinRules) ? $this->_ted->quoteInto($joinRules[0], $joinRules[1]) : $joinRules,
            'columns'   => $columns,
        );
        if (count($columns)) {
            $this->_parts['columns'] = array_merge($this->_parts['columns'], $columns);
        }
        
        return $this;
    }
    
    /**
     * Add inner join to query
     * 
     * @param <string> $tableName
     * @param <string|array> $joinRules
     * @param <array> $columns
     * @return <\TED\Query>
     */
    public function join($tableName, $joinRules, array $columns = array())
    {
        $this->_parts['join'][] = array(
            'type'      => 'JOIN',
            'table'     => $tableName,
            'on'        => is_array($joinRules) ? $this->_ted->quoteInto($joinRules[0], $joinRules[1]) : $joinRules,
            'columns'   => $columns,
        );
        if (count($columns)) {
            $this->_parts['columns'] = array_merge($this->_parts['columns'], $columns);
        }

        return $this;
    }
    
    /**
     * Build query as string
     * 
     * @return <string>
     */
    public function __toString() 
    {
        $sqlQuery = sprintf('SELECT %s',
            count($this->_parts['columns']) ? implode(', ', $this->_parts['columns']) : sprintf("%s.*", $this->_table->getName())
        );
        
        $sqlQuery .= sprintf(' FROM %s',
            $this->_ted->quoteIdentifier($this->_table->getName())
        );
        
        if (count($this->_parts['join'])) {
            foreach ($this->_parts['join'] as $join) {
                $sqlQuery .= sprintf(' %s %s ON %s', 
                    $join['type'], 
                    $join['table'],
                    $join['on']
                );
            }
        }
        
        if (count($this->_parts['where'])) {
            $sqlQuery .= ' WHERE ';
            $sqlQuery .= implode(' AND ', $this->_parts['where']);
        }
        
        if (count($this->_parts['group'])) {
            $sqlQuery .= ' GROUP BY ';
            $sqlQuery .= implode(', ', $this->_parts['group']);
        }
        
        if (count($this->_parts['order'])) {
            $sqlQuery .= ' ORDER BY ';
            $sqlQuery .= implode(', ', $this->_parts['order']);
        }
        
        return $sqlQuery;
    }
    
    /**
     * Iterator::current()
     * 
     * @return <\TED\Entry>
     */
    public function current() 
    {
        return new \TED\Entry($this->_table, $this->_row['id'], $this->_row);
    }
    
    /**
     * Iterator::key()
     * 
     * @return <int>
     */
    public function key() 
    {
        return $this->_iterator;
    }
    
    /**
     * Iterator::next()
     */
    public function next() 
    {
        $this->_iterator++;
        $this->_row = $this->_smt->fetch(\PDO::FETCH_ASSOC);
    }
    
    /**
     * Iterator::rewind()
     */
    public function rewind() 
    {
        $this->_smt = $this->_ted->getStatement($this->__toString());
        $this->_smt->execute();
        $this->_iterator = 0;
        $this->next();
    }
    
    /**
     * Iterator::valid()
     * 
     * @return <bool>
     */
    public function valid() 
    {
        return is_array($this->_row);
    }
}