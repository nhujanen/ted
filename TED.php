<?php

require_once 'TED/Entry.php';
require_once 'TED/Exception.php';
require_once 'TED/Table.php';
require_once 'TED/Query.php';

/**
 * TooEasyDatabase: Database Abstraction Layer
 * 
 * @copyright (c) 2015, Niko Hujanen
 * @version 1.00
 * @package TED
 */
class TED implements ArrayAccess
{
    protected   $_cache     = array(
        'tables'    => array(),
    );
    protected   $_pdo       = null;
    protected   $_lastID    = null;

    /**
     * Initialize TED
     * 
     * @param <array> $config [hostname, database, username, password]
     * @return <\TED>
     */
    protected function __construct(array $config) 
    {
        $this->_pdo = new \PDO(
            sprintf('%s:host=%s;dbname=%s',
                'mysql',
                $config['hostname'],
                $config['database']
            ),
            $config['username'],
            $config['password'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            )
        );
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        return $this;
    }
    
    /**
     * Cloning not allowed.
     * 
     * @throws <\TED\Exception>
     */
    private function __clone() 
    {
        throw new \TED\Exception('No cloning.');
    }
    
    /**
     * Get TED instance
     * 
     * @param <array> $config [hostname, database, username, password]
     * @return <\TED>
     */
    public static function connect(array $config)
    {
        return new static($config);
    }
    
    /**
     * Get Last generated value
     * 
     * @return <int> Last generated value
     */
    public function lastID()
    {
        return $this->_lastID;
    }
    
    /**
     * Delete a row from table with conditions
     * 
     * @param <string> $tableName   Name of table
     * @param <string> $conditions  SQL conditions
     * @return <bool>
     */
    public function delete($tableName, $conditions)
    {
        $sqlQuery = sprintf('DELETE FROM %s WHERE %s LIMIT 1',
            $this->quoteIdentifier($tableName),
            $conditions
        );
        
        return $this->_pdo->exec($sqlQuery);
    }
    
    /**
     * Insert data to table
     * 
     * @param <string> $tableName   Name of table
     * @param <array> $data         Data in [column -> value] format
     * @return <bool>
     */
    public function insert($tableName, array $data)
    {
        $sqlQuery = sprintf('INSERT INTO %s (%s) VALUES (%s)',
            $this->quoteIdentifier($tableName),
            implode(', ', array_map(array($this, 'quoteIdentifier'), array_keys($data))),
            implode(', ', array_map(array($this, 'quote'), array_values($data)))
        );
        
        $result = $this->_pdo->exec($sqlQuery);
        $this->_lastID = $this->_pdo->lastInsertId();
        
        return $result;
    }
    
    /**
     * Update data in table with conditions
     * 
     * @param <string> $tableName   Name of table
     * @param <array> $data         Data in [column -> value] format
     * @param <string> $conditions  SQL conditions
     * @return <bool>
     */
    public function update($tableName, array $data, $conditions)
    {
        $_data = array();
        foreach ($data as $_key => $_value) {
            $_data[] = sprintf('%s = %s', 
                $this->quoteIdentifier($_key),
                $this->quote($_value)
            );
        }
        
        $sqlQuery = sprintf('UPDATE %s SET %s WHERE %s',
            $this->quoteIdentifier($tableName),
            implode(', ', $_data),
            $conditions
        );
        
        return $this->_pdo->exec($sqlQuery);
    }
    
    /**
     * Insert data to table, update if exists
     * 
     * @param <string> $tableName   Name of table
     * @param <array> $data         Data in [column -> value] format
     * @return <bool>
     */
    public function insertOrUpdate($tableName, array $data)
    {
        $_data = array();
        foreach ($data as $_key => $_value) {
            $_data[] = sprintf('%s = %s', 
                $this->quoteIdentifier($_key),
                $this->quote($_value)
            );
        }
        
        $sqlQuery = sprintf('INSERT INTO %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s',
            $this->quoteIdentifier($tableName),
            implode(', ', array_map(array($this, 'quoteIdentifier'), array_keys($data))),
            implode(', ', array_map(array($this, 'quote'), array_values($data))),
            implode(', ', $_data)
        );
        
        $this->_pdo->exec($sqlQuery);
        $_lastID = $this->_pdo->lastInsertId();
        $this->_lastID = $_lastID ? $_lastID : ( array_key_exists('id', $data) ? $data['id'] : null );
        
        return true;
    }
    
    /**
     * Quote value (to string)
     * 
     * @param <mixed> $value
     * @return <string>
     */
    public function quote($value)
    {
        if (is_null($value)) {
            return 'NULL';
        } else {        
            return $this->_pdo->quote($value);
        }
    }
    
    /**
     * Quote identifier
     * 
     * @param <string> $value
     * @return <string>
     */
    public function quoteIdentifier($value)
    {
        return sprintf('`%s`', str_replace('`', '``', $value));
    }
    
    /**
     * Quote as condition
     * 
     * @param <string> $sqlQuery    SQL query with ? as placeholder for value
     * @param <string> $value       This value will be quoted to placeholders
     * @return <string>             Quoted SQL condition
     */
    public function quoteInto($sqlQuery, $value)
    {
        return str_replace('?', $this->_pdo->quote($value), $sqlQuery);
    }
    
    /**
     * Get row with SQL query
     * 
     * @param <string> $sqlQuery
     * @return <array|null>
     */
    public function getRow($sqlQuery)
    {
        return $this->_pdo->query($sqlQuery)->fetch(PDO::FETCH_ASSOC);
    }
    
    /**
     * Get all rows with SQL query
     * 
     * @param <string> $sqlQuery
     * @return <array|null>
     */
    public function getAll($sqlQuery)
    {
        return $this->_pdo->query($sqlQuery)->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Get statement for SQL query
     * 
     * @param <string> $sqlQuery
     * @return <\PDOStatement>
     */
    public function getStatement($sqlQuery)
    {
        return $this->_pdo->query($sqlQuery);
    }
    
    /**
     * See if table exists
     * 
     * @param <string> $tableName
     * @return <bool>
     */
    public function tableExists($tableName)
    {
        if (!array_key_exists($tableName, $this->_cache['tables'])) {
            $this->_cache['tables'][$tableName] = is_array($this->getRow($this->quoteInto('SHOW TABLES LIKE ?', $tableName)));
        }
        
        return $this->_cache['tables'][$tableName];
    }
    
    /**
     * See if table exists
     * 
     * @param <string> $tableName
     * @return <bool>
     */
    public function offsetExists($tableName) 
    {
        return $this->tableExists($tableName);
    }
    
    /**
     * Get table with \ArrayAccess
     * 
     * @param <string> $tableName
     * @return <\TED\Table>
     * @throws <\TED\Exception>
     */
    public function offsetGet($tableName) 
    {
        if ($this->tableExists($tableName)) {
            return new \TED\Table($this, $tableName);
        } else {
            throw new \TED\Exception("Table '{$tableName}' not found.");
        }
    }
    
    /**
     * Not allowed.
     * 
     * @throws <\TED\Exception>
     */
    public function offsetSet($tableName, $value) 
    {
        throw new \TED\Exception("Setting '{$tableName}' is not possible.");
    }
    
    /**
     * Not allowed.
     * 
     * @throws <\TED\Exception>
     */
    public function offsetUnset($tableName) 
    {
        throw new \TED\Exception("Deleting '{$tableName}' is not possible for security reasons.");
    }
    
    /**
     * Get \TED\Table
     * 
     * @param <string> $tableName
     * @return <\TED\Table>
     * @throws <\TED\Exception>
     */
    public function __get($tableName) 
    {
        if ($this->tableExists($tableName)) {
            return new \TED\Table($this, $tableName);
        } else {
            throw new \TED\Exception("Table '{$tableName}' not found.");
        }
            
    }
    
    public function __call($tableName, $arguments = array()) 
    {
        if ($this->tableExists($tableName)) {
            return new \TED\Query($this, new \TED\Table($this, $tableName));
        } else {
            throw new \TED\Exception("Table '{$tableName}' not found.");
        }
    }
    
}